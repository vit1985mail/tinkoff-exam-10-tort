import java.text.DecimalFormat;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        double steep;
        double sum = 0.000001;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();// ���������� ������ ��������������
        double min = 1001;
        double max = -1000;
//        4 0 0 0 2 2 2 2 0
//        3 2 1 4 5 7 8
//        6 1 3 2 4 4 5 5 5 6 4 4 2
        //  3 1 1 4 5 4 1

        double[] x = new double[n + 1];
        double[] y = new double[n + 1];

        for (int i = 0; i < n; i++) {
            x[i] = scanner.nextInt();
            if (x[i] < min) min = x[i];
            if (x[i] > max) max = x[i];
            y[i] = scanner.nextInt();
        }
        x[x.length - 1] = x[0];
        y[y.length - 1] = y[0];
        steep = min + sum;
        scanner.close();

        String squaerFigure = new DecimalFormat("#0.000000").format(findSquaer(x, y) / 2);

        for (int i = (int) min; i < (max - min) * 1000000; i++) {

            double squaerFind = search_Y_One(steep, x, y);

     //       System.out.println(squaerFigure);
            String squaerFind1 = new DecimalFormat("#0.000000").format(squaerFind);
  //          System.out.println(squaerFind1);
            if (squaerFind1.equals(squaerFigure)) {
                System.out.println(new DecimalFormat("#0.000000").format(steep));
                break;
            }

 //          System.out.println();
            steep += sum;
        }

    }

    private static double search_Y_One(double numberX, double[] pointX, double[] pointY) {
        int count = 0;
        int numberIndex = 0;
        for (int i = 0; i < pointX.length - 1; i++) {

            if (pointX[i] < numberX) {
                count++;
            }
        }

        double[] newX = new double[count + 3];
        double[] newY = new double[count + 3];

        for (int i = 0; i < pointX.length; i++) {
            if (numberX <= pointX[i]) {
                numberIndex = i;
                newX[i] = numberX;
                newY[i] = (((numberX - pointX[i - 1]) * (pointY[i] - pointY[i - 1])) / (pointX[i] - pointX[i - 1])) + pointY[i - 1];

                break;
            }
            newX[i] = pointX[i];
            newY[i] = pointY[i];
        }

        int countIndex = newX.length - 1;
        for (int i = pointX.length - 1; i > 0; i--) {
            ;
            if (numberX < pointX[i]) {
                newX[numberIndex + 1] = numberX;
                newY[numberIndex + 1] = (((numberX - pointX[i + 1]) * (pointY[i] - pointY[i + 1])) / (pointX[i] - pointX[i + 1])) + pointY[i + 1];
//                newY[numberIndex+1]=(((pointX[i]*pointY[i+1] - pointY[i]*pointX[i+1])*(0-5))-((pointY[i]-pointY[i+1])*(numberX*5)))/
//                        ((pointX[i]-pointX[i+1])*(0-5));
                //            System.out.println("y2: " + newY[countIndex]);
                break;
            } else {
                newX[countIndex] = pointX[i];
                newY[countIndex] = pointY[i];
                countIndex--;
            }
        }

        return findSquaer(newX, newY);
    }

    private static double findSquaer(double[] pointX, double[] pointY) {
        double squaresum = 0.0;
        for (int i = 0; i < pointX.length - 1; i++) {

            if (i == pointX.length - 2) {
                squaresum += pointX[i] * pointY[0] - pointX[0] * pointY[i];
            } else {
                squaresum += pointX[i] * pointY[i + 1] - pointX[i + 1] * pointY[i];
            }
        }
        return Math.abs(squaresum) / 2;
    }
}



